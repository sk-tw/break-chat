import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Header from './components/header';
import PeerContextProvider from './contexts/peer';
import HomePage from './pages';
import RoomPage from './pages/room';

function App() {
    return (
        <PeerContextProvider>
            <BrowserRouter>
                <Header />
                <Routes>
                    <Route path="room/" element={<RoomPage />} />
                    <Route path="/" element={<HomePage />} />
                </Routes>
            </BrowserRouter>
        </PeerContextProvider>
    );
}

export default App;
