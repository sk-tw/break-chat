import React, { useContext } from 'react';
import { Card } from 'react-bootstrap';
import { PeerContext } from '../contexts/peer';
// import logo from '../logo.svg';

export default function Home() {
    const stream = useContext(PeerContext)[2];
    const handleRef = (video) => {
        if (!video) return;

        video.addEventListener('loadedmetadata', () => {
            video.play();
        });
        // eslint-disable-next-line no-param-reassign
        video.srcObject = stream;
    };
    return (
        <div className="App">
            <header className="App-header">
                <center>
                    <Card style={{ maxWidth: '500px' }}>
                        {/* eslint-disable-next-line jsx-a11y/media-has-caption */}
                        <video style={{ borderRadius: '5px', maxWidth: '100%' }} ref={handleRef} muted />
                    </Card>
                </center>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Break Chat
                </a>
            </header>
        </div>
    );
}
