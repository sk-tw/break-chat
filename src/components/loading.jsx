import React from 'react';
import { Triangle } from 'react-loader-spinner';

export default function Loading({ full }) {
    return (
        <div style={{
            display: 'flex',
            alignContent: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            minHeight: full ? `${window.screen.height - 200}px` : 'auto',
        }}
        >
            <Triangle height="100px" width="100px" color="red" />
        </div>
    );
}
