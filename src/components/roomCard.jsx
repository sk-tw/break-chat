import React, { useContext, useEffect, useState } from 'react';
import { Card } from 'react-bootstrap';
import { PeerContext } from '../contexts/peer';
import Loading from './loading';

function Video(props) {
    const {
        style, className, stream, muted,
    } = props;
    const handleRef = (video) => {
        if (!video) return;

        video.addEventListener('loadedmetadata', () => {
            video.play();
        });
        // eslint-disable-next-line no-param-reassign
        video.srcObject = stream;
    };

    // eslint-disable-next-line jsx-a11y/media-has-caption
    return <video style={style} className={className} ref={handleRef} muted={muted} preload="metadata" />;
}

function MyVideoCard({ myStream }) {
    return (
        <Card.Img as={Video} stream={myStream} alt="Card image" muted />
    );
}

function PeerVideoCard({ myStream, peerId, peer }) {
    const [peerStream, setPeerStream] = useState(null);
    useEffect(() => {
        const call = peer.call(peerId, myStream);
        if (!call) return () => {};
        call.on('stream', setPeerStream);
        return () => { call.close(); };
    }, []);
    return (
        <Card.Img as={peerStream ? Video : Loading} stream={peerStream} alt="Card image" />
    );
}
export default function RoomCard({ peerId }) {
    const [userId, peer, stream] = useContext(PeerContext);
    const mine = String(peerId) === String(userId);

    return (
        <Card className="bg-dark text-white">
            {mine
                ? <MyVideoCard myStream={stream} />
                : <PeerVideoCard myStream={stream} peer={peer} peerId={peerId} />}
            <Card.ImgOverlay>
                <Card.Title>{mine ? 'You' : ''}</Card.Title>
            </Card.ImgOverlay>
        </Card>
    );
}
