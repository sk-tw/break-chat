import React, { useContext, useEffect, useState } from 'react';
import { Button, Col, Row } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import RoomCard from './roomCard';
import { RoomContext } from '../contexts/room';

export default function Room() {
    const [room, leaveRoom] = useContext(RoomContext);
    const [users, setUsers] = useState([]);

    const navigate = useNavigate();

    useEffect(() => {
        setUsers(room?.users || []);
    }, [room]);

    const leaveHandler = () => {
        leaveRoom();
        navigate('/');
    };

    return (
        <div>
            <Row style={{ minHeight: `${window.screen.height - 200}px` }}>
                {users.map((userId) => (
                    <Col key={userId} sm={6} md={4} style={{ padding: '5px' }}>
                        <RoomCard peerId={userId} />
                    </Col>
                ))}
            </Row>
            <div style={{ display: 'flex', alignContent: 'center', justifyContent: 'center' }}>
                <Button variant="danger" onClick={leaveHandler}>Leave</Button>
            </div>
        </div>
    );
}
