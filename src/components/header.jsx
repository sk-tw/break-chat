import { getApp } from 'firebase/app';
import {
    get, getDatabase, ref, update,
} from 'firebase/database';
import React, { useContext } from 'react';
import {
    Button, Container, Nav, Navbar,
} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import uniqid from 'uniqid';
import { PeerContext } from '../contexts/peer';

export default function Header() {
    const [peerId] = useContext(PeerContext);
    const navigate = useNavigate();
    const onCreate = () => {
        const db = getDatabase(getApp());
        const id = uniqid();
        update(ref(db, 'rooms'), {
            [id]: {
                createdBy: peerId,
                users: [],
            },
        }).then(() => {
            navigate(`/room?roomId=${id}`);
        }).catch((err) => {
            // eslint-disable-next-line no-console
            console.log(err);
            Swal.fire({
                title: 'Error',
                text: 'Unable to create room',
                icon: 'error',
            });
        });
    };
    const onJoin = () => {
        const db = getDatabase(getApp());
        get(ref(db, 'rooms')).then((snap) => {
            if (!snap.exists()) {
                throw new Error('No rooms to join');
            }
            const rooms = Object.keys(snap.val());
            if (rooms.length === 0) {
                throw new Error('No rooms available');
            }
            navigate(`/room?roomId=${rooms[Math.floor(Math.random() * rooms.length)]}`);
        }).catch((err) => {
            // eslint-disable-next-line no-console
            console.log(err);
            Swal.fire({
                title: 'Error',
                text: String(err),
                icon: 'error',
            });
        });
    };
    return (
        <Navbar bg="dark" variant="dark">
            <Container>
                <Navbar.Brand href="/">Break Chat</Navbar.Brand>
                <Nav className="me-auto">
                    <Button variant="dark" onClick={onCreate}>Create</Button>
                </Nav>
                <Button variant="info" onClick={onJoin}>Join</Button>
            </Container>
        </Navbar>
    );
}
