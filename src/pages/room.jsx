import React from 'react';
import Room from '../components/room';
import RoomContextProvider from '../contexts/room';

export default function RoomPage() {
    return (
        <RoomContextProvider>
            <Room />
        </RoomContextProvider>
    );
}
