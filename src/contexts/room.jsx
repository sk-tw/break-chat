import React, {
    useContext, useEffect, useMemo, useState,
} from 'react';
import { getApp } from 'firebase/app';
import {
    getDatabase, onValue, ref, remove, set, off,
} from 'firebase/database';
import { useSearchParams, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import Loading from '../components/loading';
import { PeerContext } from './peer';

export const RoomContext = React.createContext(null);
let db = null;
let dbRef = null;
export default function RoomContextProvider({ children }) {
    const [peerId] = useContext(PeerContext);
    const [room, setRoom] = useState(null);
    const [loading, setLoading] = useState(true);
    const [params] = useSearchParams();
    const roomId = params.get('roomId');
    const updateRoom = (updatedRoom) => {
        setRoom(updatedRoom);
        set(dbRef, updatedRoom);
    };
    const joinRoom = (newRoom) => {
        if (newRoom?.users?.includes(String(peerId))) {
            return setRoom(newRoom);
        }
        // eslint-disable-next-line no-param-reassign
        newRoom.users = Array.from(new Set([...(newRoom?.users || []), peerId])).filter(Boolean);
        return updateRoom(newRoom);
    };
    const leaveRoom = () => {
        if (!dbRef) return;
        off(dbRef);
        if (!room) return;
        if (String(peerId) === String(room?.createdBy)) {
            remove(dbRef);
        } else {
            const users = room.users.filter((id) => String(id) !== String(peerId));
            set(dbRef, { ...room, users });
        }
        setRoom(null);
    };
    useEffect(() => {
        setLoading(true);
        if (!db) { db = (getDatabase(getApp())); }
        leaveRoom();
        dbRef = ref(db, `rooms/${roomId}`);

        onValue(dbRef, (snapshot) => {
            if (!snapshot.exists()) {
                setRoom(null);
                Swal.fire({
                    title: 'Room not found/closed.',
                    icon: 'error',
                    timer: 3000,
                }).then(() => {
                    setLoading(false);
                });
                return;
            }
            joinRoom(snapshot.val());
            setLoading(false);
        });
    }, [roomId]);

    useEffect(() => {
        const leaveListener = (e) => {
            e.preventDefault();
            leaveRoom();
            return 'You have unsaved changes.';
        };
        window.onbeforeunload = leaveListener;
        return () => {
            window.onbeforeunload = null;
        };
    });

    return (
        <RoomContext.Provider value={useMemo(() => [room, leaveRoom], [room])}>
            {loading ? <Loading full /> : <div>{room ? children : <Navigate to="/" />}</div>}
        </RoomContext.Provider>
    );
}
