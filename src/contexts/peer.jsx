import Peer from 'peerjs';
import React, { useEffect, useMemo, useState } from 'react';
import Swal from 'sweetalert2';
import Loading from '../components/loading';

export const PeerContext = React.createContext([]);

export default function PeerContextProvider({ children }) {
    const [peer, setPeer] = useState(null);
    const [peerId, setPeerId] = useState(null);
    const [stream, setStream] = useState(null);
    useEffect(() => {
        const newPeer = new Peer();
        setPeer(newPeer);
        newPeer.on('open', (id) => {
            setPeerId(id);
        });
        navigator.mediaDevices.getUserMedia({
            video: true,
            audio: true,
        }).then((newStream) => {
            setStream(newStream);
            newPeer.on('call', (call) => {
                call.answer(newStream);
            });
        }).catch((err) => {
            // eslint-disable-next-line no-console
            console.log(err);
            Swal.fire({
                title: 'Error Occured',
                text: 'Unable to capture video or audio!!',
                icon: 'error',
            }).then(() => window.location.reload());
        });
    }, []);

    return (
        <PeerContext.Provider value={useMemo(() => [peerId, peer, stream], [peerId, stream])}>
            {peerId && stream ? children : <Loading full />}
        </PeerContext.Provider>
    );
}
